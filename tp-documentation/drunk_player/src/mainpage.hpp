/// \mainpage Documentation de drunk_player
///
/// \brief Drunk_player est un système de lecture de vidéos qui a trop bu. Il lit les vidéos contenues dans un dossier par morceaux, aléatoirement et parfois en transformant l'image. 
///
///
/// \brief Drunk_player utilise la bibliothèque de traitement d'image <a href="opencv.org">OpenCV</a> et est composé :
///
/// \li d'une bibliothèque (drunk_player) contenant le code de base
/// \li d'un programme graphique (drunk_player_gui) qui affiche le résultat à l'écran
/// \li d'un programme console (drunk_player_cli) qui sort le résultat dans un fichier

