#include <guess/guess.hpp>

#include <iostream>

void deviner() {  

    int y;
    int nbrDebut = 1; 

    //determine le chiffre à les chiffre 
    int nbrJouer = rand()%100;
    bool rep = false;

    std::cout << "Deviner un chiffre de 1 à 100" << std::endl;
    std::cin >> y;

    //nombre different
    while(y != nbrJouer && nbrDebut < 5) {
        if (y < nbrJouer) {
            std::cout << "Up" << std::endl;
            std::cin >> y;
        } else {
            std::cout << "Down" << std::endl; 
            std::cin >> y;
        }
        nbrDebut+=1;

        //le cas ou nombre sont egaux
        if(y == nbrJouer && nbrDebut < 5) rep = true;
    }

    //si reponse vrai ou faux
    if(rep == true) {
        std::cout << "Congratilation" << std::endl;
    } else {
        std::cout << "Game-Over"<< std::endl;
    }
}

int main() {

    deviner();
    return 0;

    // std::cout << "this is guess" << std::endl;
    //std::cout << mul2(21) << std::endl;
}