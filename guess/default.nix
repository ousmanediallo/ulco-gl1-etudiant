{ pkgs ? import <nixpkgs> {} }:

with pkgs; clangStdenv.mkDerivation {
    name = "guess";
    src = ./.;

    nativeBuildInputs = [
        boost
        cmake
        doxygen
        pkg-config
        opencv_gtk
    ];

    doCheck = true;
}


