cmake_minimum_required( VERSION 3.0 )
project( play-videos )

gtkmm-2.4 libvlc
boost (system filesystem)

add_executable( play-videos
    src/Filesystem.cpp 
    src/PlayerWindow.cpp 
    src/play-videos.cpp )

install( TARGETS play-videos DESTINATION bin )

