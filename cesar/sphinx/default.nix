with import <nixpkgs> {};

clangStdenv.mkDerivation {
  name = "cesar";
  src = ./.;

  buildInputs = [
    boost
    cmake
    doxygen
    pkg-config
    opencv_gtk
  ];

}
