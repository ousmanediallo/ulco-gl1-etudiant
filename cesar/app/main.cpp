#include <cesar/cesar.hpp>

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>

//fonction de cesar 
void cesar(char str[], int shift) {
  int i = 0;
    while (str[i] != '\0') {
        if (str[i] >= 'A' && str[i]<= 'Z') {
            char c = str[i] - 'A';
            c += shift;
            c = c % 26;
            str[i] = c + 'A';
        }
        i++;
    }
    std::cout << str << std::endl;
}

//main
int main()
{   FILE * f;
    f = fopen ("Input.txt", "rb");
    char str[] = f;
    cesar(str, 2);
    return 0;
}

/*
int main() {
    int main() {
    std::string str;
    std::string line;
    while (std::getline(std::cin, line))
    {
        str += line + "\n";
    }
    std::cout << str << std::endl;
    return 0;
}*/